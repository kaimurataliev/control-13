const mongoose = require('mongoose');
const config = require('./config');

const User = require('./models/User');
const Place = require('./models/Place');
const Review = require('./models/Review');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
    try {
        await db.dropCollection('places');
        await db.dropCollection('users');
        await db.dropCollection('images');
        await db.dropCollection('reviews');
    } catch (e) {
        console.log('Collections were not present, skipping drop...');
    }

    console.log('collection is dropped');

    const [kairat, admin, user2] = await User.create({
        username: 'kairat',
        password: '123',
    }, {
        username: 'admin',
        password: 'admin',
        role: 'admin'
    }, {
        username: 'user2',
        password: '123'
    });

    console.log('user created');

    const [navat, cosmo, iTattractor] = await Place.create({
        userId: kairat._id,
        image: 'navat.jpg',
        title: 'Navat cafe',
        description: 'Some description of this cafe',
    }, {
        userId: kairat._id,
        image: 'cosmo.jpeg',
        title: 'Cosmopark',
        description: 'Description of cosmopark',
    }, {
        userId: user2._id,
        image: 'it.jpg',
        title: 'It Attractor',
        description: 'Some description!'
    });

    const [review1, review2, review3] = await Review.create({
        userId: kairat._id,
        placeId: cosmo._id,
        review: 'Review'
    }, {
        userId: admin._id,
        placeId: navat._id,
        review: 'Review for navat'
    }, {
        userId: user2._id,
        placeId: iTattractor._id,
        review: 'some review 2'
    });

    console.log('place created');

    db.close();
});