const express = require('express');
const auth = require('../middleware/auth');
const config = require('../config');
const Image = require('../models/Image');
const multer = require('multer');
const nanoid = require('nanoid');
const path = require('path');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = () => {
    const router = express.Router();

    router.post('/newImage', [upload.single('name')], async (req, res) => {
        try {
            const image = new Image();
            image.name = req.file.filename;
            image.userId = req.body.userId;
            image.placeId= req.body.placeId;
            image.save();

            const images = await Image.find({placeId: req.body.placeId});
            res.send(images);

        } catch (error) {
            console.log(error, 'images.js::: ADD IMAGE METHOD');
            res.status(500).send({message: 'You can not add image at this moment'});
        }
    });

    router.get('/', async (req, res) => {
        try {
            const id = req.query.id;
            const images = await Image.find({placeId: id});
            res.send(images);
        } catch (error) {
            console.log(error, 'images.js:::GET METHOD');
            res.status(500).send({message: 'Can not fetch images now'})
        }
    });

    return router;
};

module.exports = createRouter;