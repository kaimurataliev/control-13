const express = require('express');
const User = require('../models/User');
const auth = require('../middleware/auth');
const config = require('../config');
const request = require('request-promise-native');
const Place = require('../models/Place');
const nanoid = require('nanoid');
const Review = require('../models/Review');

const createRouter = () => {
    const router = express.Router();

    router.post('/addReview', async (req, res) => {
        try {
            const review = new Review();
            review.userId = req.body.userId;
            review.placeId = req.body.placeId;
            review.review = req.body.review;
            review.save();

            const reviews = await Review.findOne({placeId: req.body.placeId});
            res.send(reviews);
        } catch (error) {
            console.log(error, 'images.js::: ADD IMAGE METHOD');
            res.status(500).send({message: 'You can not add image at this moment'});
        }
    });

    router.get('/', async (req, res) => {
        try {
            const id = req.query.id;
            const reviews = await Review.find({placeId: id}).populate('userId');
            console.log(reviews);
            res.send(reviews);

        } catch (error) {
            console.log(error);
            res.status(500).send({message: 'Error occured'});
        }
    });

    return router;
};

module.exports = createRouter;