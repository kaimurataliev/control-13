const express = require('express');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const config = require('../config');
const Place = require('../models/Place');
const multer = require('multer');
const nanoid = require('nanoid');
const path = require('path');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = () => {
    const router = express.Router();

    router.post('/addNewPlace', [auth, upload.single('image')], async (req, res) => {
        try {
            const place = new Place();

            if (req.file.filename === undefined) {
                place.title = req.body.title;
                place.description = req.body.description;
                place.userId = req.body.userId;
            } else {
                place.image = req.file.filename;
                place.title = req.body.title;
                place.description = req.body.description;
                place.userId = req.body.userId;
            }
            place.save();
            const places = await Place.find().populate('userId');
            res.send(places);
        } catch (error) {
            console.log(error, "places.js:::ADD NEW PLACE METHOD");
            res.status(500).send({message: 'You can not add new place at this moment!'});
        }
    });

    router.get('/', async (req, res) => {
        try {
            const places = await Place.find().populate('userId');
            res.send(places);
        } catch (error) {
            console.log(error, 'places.js:::GET PLACES METHOD');
            res.status(500).send({message: "Can not get places at this moment"});
        }
    });


    router.get('/getPlaceById', async (req, res) => {
        try {
            const id = req.query.id;
            const place = await Place.findOne({_id: id}).populate('userId');
            res.send(place);
        } catch (error) {
            console.log(error);
            res.status(500).send({message: 'error occured!'})
        }
    });

    router.delete('/deletePlace/:id', [auth, permit('admin')], async (req, res) => {
        console.log(req.params.id);
        try {
            const id = req.params.id;
            const place = await Place.findOne({_id: id});
            place.remove();
            const places = await Place.find();
            res.send(places);
        } catch (e) {
            console.log(e, 'places.js:::DELETE METHOD');
            res.status(500).send({message: 'can not delete at this moment'});
        }
    });

    return router;
};

module.exports = createRouter;