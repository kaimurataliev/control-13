const mongoose = require('mongoose');
const config = require('../config');
const nanoid = require('nanoid');

const Schema = mongoose.Schema;

const ImageSchema = new Schema({
    placeId: {
        type: Schema.Types.ObjectId,
        ref: 'Place',
        required: true
    },
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    name: {
        type: String
    }
});


const Image = mongoose.model('Image', ImageSchema);

module.exports = Image;