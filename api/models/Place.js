const mongoose = require('mongoose');
const config = require('../config');
const nanoid = require('nanoid');

const Schema = mongoose.Schema;

const PlaceSchema = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    image: {
        type: String,
    },
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    }
});


const Place = mongoose.model('Place', PlaceSchema);

module.exports = Place;