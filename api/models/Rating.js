const mongoose = require('mongoose');
const config = require('../config');
const nanoid = require('nanoid');

const Schema = mongoose.Schema;

const RatingSchema = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    placeId: {
        type: Schema.Types.ObjectId,
        ref: 'Place',
        required: true
    },
    kitchenRate: {
        type: String,
    },
    ServiceRate: {
        type: String,
    },
    interior: {
        type: String,
    }
});


const Rating = mongoose.model('Rating', RatingSchema);

module.exports = Rating;