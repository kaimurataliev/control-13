const urls = require('../urls');
const findAndClick = require('../../common/findAndClick');
const findAndSetValue = require('../../common/findAndSetValue');
const confirm = require('../../common/confirmLoginOrRegister');
const tryClick = require('../../common/TryClick');
const trySetValue = require('../../common/trySetValue');
const confirmExisting = require('../../common/confirmExisting');


module.exports = function () {

    this.When(/^я создаю новое заведение$/, function () {
        browser.url(urls.loginUrl);
        trySetValue(`//input[@name='username']`, 'admin');
        trySetValue(`//input[@name='password']`, 'admin');
        tryClick(`//button[text()="Login"]`);
        tryClick(`//a[text()="Add New Place"]`);
        tryClick(`//input[@type='checkbox']`);
        trySetValue(`//input[@name='title']`, 'New place');
        trySetValue(`//input[@name='description']`, 'description');
        return tryClick(`//button[text()="Save"]`);
    });

    this.Then(/^я вижу что заведение появилось в списке$/, function () {
        confirmExisting(``)
    })
    
};
