const baseUrl = 'http://localhost:3000';

module.exports = {
    mainUrl: baseUrl,
    loginUrl: baseUrl + '/login',
};