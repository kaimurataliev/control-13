import axios from '../../axios-api';

export const FETCH_ALL_REVIEWS = 'FETCH_ALL_REVIEWS';


export const fetchAllReviewsSuccess = data => {
    return {type: FETCH_ALL_REVIEWS, data};
};

export const fetchReviews = (id) => {
    return dispatch => {
        return axios.get(`/reviews?id=${id}`).then(response => {
            dispatch(fetchAllReviewsSuccess(response.data));
        }, error => {
            console.log(error);
        })
    }
};

export const addReview = (review) => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        const headers = {'Token': token};
        return axios.post('/reviews/addReview', review, {headers}).then(response => {
            dispatch(fetchAllReviewsSuccess(response.data));
        })
    }
};