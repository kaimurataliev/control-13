import axios from '../../axios-api';
import {NotificationManager} from 'react-notifications';

export const FETCH_IMAGES_SUCCESS = 'FETCH_IMAGES_SUCCESS';

export const fetchImagesSuccess = data => {
    return {type: FETCH_IMAGES_SUCCESS, data};
};

export const fetchImages = (id) => {
    return dispatch => {
        return axios.get(`/images?id=${id}`).then(response => {
            dispatch(fetchImagesSuccess(response.data));
        }, error => {
            NotificationManager.error('error');
        })
    }
};

export const addImage = (image) => {
    return dispatch => {
        return axios.post('/images/newImage', image).then(response =>{
            dispatch(fetchImagesSuccess(response.data));
        }, error => {
            NotificationManager.error('Error');
        })
    }
};