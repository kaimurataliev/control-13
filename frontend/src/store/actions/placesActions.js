import axios from '../../axios-api';
import {push} from 'react-router-redux';

export const ADD_NEW_PLACE_FAILURE = 'ADD_NEW_PLACE_FAILURE';
export const FETCH_PLACES_SUCCESS = 'FETCH_PLACES_SUCCESS';
export const FETCH_ALL_PLACES_FAILURE = 'FETCH_ALL_PLACES_FAILURE';
export const FETCH_PLACE_BY_ID_SUCCESS = 'FETCH_PLACE_BY_ID_SUCCESS';
export const FETCH_PLACE_BY_ID_FAILURE = 'FETCH_PLACE_BY_ID_FAILURE';

export const fetchPlacesSuccess = (data) => {
    return {type: FETCH_PLACES_SUCCESS, data};
};

export const fetchPlacesFailure = (error) => {
    return {type: FETCH_ALL_PLACES_FAILURE, error};
};

export const fetchPlaces = () => {
    return dispatch => {
        return axios.get('/places').then((response) => {
            dispatch(fetchPlacesSuccess(response.data))
        }).catch(error => {
            dispatch(fetchPlacesFailure());
        })
    }
};

export const fetchPlaceByIdSuccess = (data) => {
    return {type: FETCH_PLACE_BY_ID_SUCCESS, data};
};

export const fetchPlaceByIdFailure = (error) => {
    return {type: FETCH_PLACE_BY_ID_FAILURE, error};
};

export const fetchPlaceById = (id) => {
    return (dispatch) => {
        return axios.get(`/places/getPlaceById?id=${id}`).then(response => {
            dispatch(fetchPlaceByIdSuccess(response.data))
        }).catch(error => {
            dispatch(fetchPlaceByIdFailure(error.response.data.message));
        })
    }
};


export const addNewPlaceFailure = (error) => {
    return {type: ADD_NEW_PLACE_FAILURE, error};
};

export const addNewPlace = (place) => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        const headers = {'Token': token};
        return axios.post('/places/addNewPlace', place, {headers}).then((response) => {
            dispatch(push('/'));
            dispatch(fetchPlacesSuccess(response.data));
        }, error => {
            dispatch(addNewPlaceFailure(error.response.data.message));
        })
    }
};


export const deletePlace = (id) => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        const headers = {'Token': token};
        return axios.delete(`/places/deletePlace/${id}`, {headers}).then((response) => {
            dispatch(fetchPlacesSuccess(response.data));
        })
    }
};

export const getPlaceByIdSuccess = (data) => {
    return {type: FETCH_PLACE_BY_ID_SUCCESS, data};
};
