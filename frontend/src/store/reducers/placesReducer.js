import {FETCH_ALL_PLACES_FAILURE, FETCH_PLACES_SUCCESS, FETCH_PLACE_BY_ID_FAILURE , FETCH_PLACE_BY_ID_SUCCESS, ADD_NEW_PLACE_FAILURE} from '../actions/placesActions';

const initialState = {
    places: [],
    placeById: [],
    error: null
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PLACES_SUCCESS:
            return {...state, places: action.data};

        case FETCH_ALL_PLACES_FAILURE:
            return {...state, error: action.error};

        case FETCH_PLACE_BY_ID_SUCCESS:
            return {...state, placeById: action.data};

        case FETCH_PLACE_BY_ID_FAILURE:
            return {...state, error: action.error};

        case ADD_NEW_PLACE_FAILURE:
            return {...state, error: action.error};

        default:
            return state;
    }
};

export default reducer;