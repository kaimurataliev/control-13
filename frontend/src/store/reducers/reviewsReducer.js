import {FETCH_ALL_REVIEWS} from '../actions/reviewsActions';

const initialState = {
    reviews: []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ALL_REVIEWS:
            return {...state, reviews: action.data};

        default:
            return state;
    }
};

export default reducer;