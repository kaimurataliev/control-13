import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {fetchPlaceById} from "../../store/actions/placesActions";
import {Label, Image, Button, Jumbotron, Col, ControlLabel, FormControl, FormGroup, Form} from 'react-bootstrap';
import {addImage, fetchImages} from "../../store/actions/imagesActions";
import './MoreInfo.css';
import {addReview, fetchReviews} from "../../store/actions/reviewsActions";


class MoreInfo extends Component {

    state = {
        image: {
            userId: '',
            placeId: '',
            name: ''
        },
        review: {
            review: '',
            userId: '',
            placeId: ''
        }
    };


    componentDidMount() {
        const id = this.props.match.params.id;
        this.props.fetchPlaceById(id).then(() => {
            this.setState({
                image: {
                    ...this.state.image,
                    userId: this.props.placeById.userId._id,
                    placeId: this.props.placeById._id
                },
                review: {
                    ...this.state.review,
                    userId: this.props.placeById.userId._id,
                    placeId: this.props.placeById._id
                }

            });
        });
        this.props.fetchImages(id);

        this.props.fetchReviews(id);
    }

    fileChangeHandler = event => {
        this.setState({
            image: {
                ...this.state.image,
                name: event.target.files[0]
            }
        });
    };

    inputChangeHandler = event => {
        this.setState({
            review: {
                ...this.state.review,
                [event.target.name]: event.target.value
            }
        });
    };


    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();
        Object.keys(this.state.image).forEach(key => {
            formData.append(key, this.state.image[key]);
        });
        this.props.addImage(formData);
    };


    sendReview = (event) => {
        event.preventDefault();
        this.props.addReview(this.state.review);
    };

    render() {
        return (
            this.props.placeById && <Fragment>
                <Label className='label'>{this.props.placeById.title}</Label>
                <div className='heading'>
                    <Image className='heading-image'
                           src={`http://localhost:8000/uploads/${this.props.placeById.image}`}/>
                    <p>{this.props.placeById.description}</p>
                    <h2>This place was uploaded
                        by: {this.props.placeById.userId && this.props.placeById.userId.username} </h2>
                </div>

                <Label className='label'>Gallery</Label>
                <div className='images-container'>
                    {this.props.images && this.props.images.map((image, index) => {
                        return (
                            <div key={index} className='image'>
                                <Image className='imageSize' src={`http://localhost:8000/uploads/${image.name}`}/>
                            </div>
                        )
                    })}
                </div>

                <Label className='label'>Reviews</Label>
                <div className='reviews-block'>
                    {!this.props.reviews ? null : this.props.reviews.map((review, index) => {
                        return (
                            <Jumbotron className='review' key={index}>
                                <h4>{review.userId.username}</h4>
                                <p>{review.review}</p>
                            </Jumbotron>
                        )
                    })}
                </div>

                {!this.props.user ? <div>If you want to add reviews and photos, please log in</div> :
                    <Fragment>
                        <Label className='label'>Review form</Label>
                        <div className='review-form'>
                            <Form onSubmit={this.sendReview} horizontal>

                                <FormGroup controlId="review">
                                    <Col componentClass={ControlLabel} sm={4}>
                                        Review
                                    </Col>
                                    <Col sm={10} md={8}>
                                        <FormControl
                                            className='review'
                                            required
                                            type="text"
                                            name="review"
                                            onChange={this.inputChangeHandler}
                                        />
                                    </Col>
                                </FormGroup>

                                <label htmlFor="foodRate">Quality of food</label>
                                <select name="food" id="foodRate">
                                    <option value="5">5</option>
                                    <option value="4">4</option>
                                    <option value="3">3</option>
                                    <option value="2">2</option>
                                    <option value="1">1</option>
                                </select>

                                <label htmlFor="interior">Interior</label>
                                <select name="interior" id="interior">
                                    <option value="5">5</option>
                                    <option value="4">4</option>
                                    <option value="3">3</option>
                                    <option value="2">2</option>
                                    <option value="1">1</option>
                                </select>

                                <label htmlFor="service">Service</label>
                                <select name="service" id="service">
                                    <option value="5">5</option>
                                    <option value="4">4</option>
                                    <option value="3">3</option>
                                    <option value="2">2</option>
                                    <option value="1">1</option>
                                </select>

                                <Button type='submit'>
                                    Add review
                                </Button>
                            </Form>
                        </div>
                    </Fragment>
                }

                {!this.props.user ? null : <div className='image-form'>
                    <Form onSubmit={this.submitFormHandler} horizontal>

                        <FormGroup controlId="image">
                            <Col componentClass={ControlLabel} sm={4}>
                                Image
                            </Col>
                            <Col sm={10} md={6}>
                                <FormControl
                                    required
                                    type="file"
                                    name="name"
                                    onChange={this.fileChangeHandler}
                                />
                            </Col>
                        </FormGroup>

                        <Button type='submit'>
                            Add image
                        </Button>
                    </Form>
                </div>}


            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        placeById: state.places.placeById,
        images: state.images.images,
        user: state.users.user,
        reviews: state.reviews.reviews
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchPlaceById: (id) => dispatch(fetchPlaceById(id)),
        fetchImages: (id) => dispatch(fetchImages(id)),
        addImage: (image) => dispatch(addImage(image)),
        fetchReviews: (id) => dispatch(fetchReviews(id)),
        addReview: (review) => dispatch(addReview(review))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(MoreInfo)