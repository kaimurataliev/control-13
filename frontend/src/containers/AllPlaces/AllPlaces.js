import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {deletePlace, fetchPlaces} from "../../store/actions/placesActions";
import {Jumbotron, Image, Button} from 'react-bootstrap';
import './AllPlaces.css';
import {Link} from 'react-router-dom';

class AllPlaces extends Component {

    componentDidMount() {
        this.props.fetchPlaces();
    }

    deleteHandler = (event, id) => {
        event.preventDefault();
        this.props.deletePlace(id).then(() => {
            this.props.fetchPlaces();
        })
    };

    render() {
        console.log(this.props.places);
        return (
            <div className='flex-container'>
                {this.props.places && this.props.places.map((place, index) => {
                    return (
                        <Jumbotron className='item' key={index}>
                            <h2>{place.title}</h2>
                            <p>{place.description}</p>
                            <Image className='image-all' src={'http://localhost:8000/uploads/' + place.image}/>

                            <Link className='view-more' to={`/moreInfo/${place._id}`}>View More</Link>
                            {this.props.user && this.props.user.role !== 'admin' || !this.props.user ? null : <Button onClick={(e) => this.deleteHandler(e, place._id)}>
                                Delete Place
                            </Button>}
                        </Jumbotron>
                    )
                })}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        user: state.users.user,
        places: state.places.places
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchPlaces: () => dispatch(fetchPlaces()),
        deletePlace: (id) => dispatch(deletePlace(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AllPlaces);