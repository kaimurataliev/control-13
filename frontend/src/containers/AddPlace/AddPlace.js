import React, {Component} from 'react';
import {Button, Col, ControlLabel, Form, FormControl, FormGroup, Label} from "react-bootstrap";
import {Redirect} from 'react-router-dom';
import {connect} from 'react-redux';
import {addNewPlace} from "../../store/actions/placesActions";

class AddPlace extends Component {

    state = {
        title: '',
        description: '',
        image: '',
        checked: false
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });
        console.log(formData);
        this.props.addNewPlace(formData);
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value, userId: this.props.user.user._id
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    checkState = () => {
        if (this.state.title.length <= 0 && this.state.description.length <= 0) {
            return true
        } else {
            return false
        }

    };

    checkInput = (event) => {
        return this.setState({checked: event.target.checked})
    };

    checkStateInput = () => {
        if(this.state.checked === false) {
            return true
        } else {
            return false
        }
    };


    render() {

        if (!this.props.user) {
            return <Redirect to="/register"/>
        }

        return (
            <Form horizontal onSubmit={this.submitFormHandler}>

                <FormGroup controlId="title">
                    <Col componentClass={ControlLabel} sm={2}>
                        Title
                    </Col>
                    <Col sm={10}>
                        <FormControl
                            disabled={this.checkStateInput()}
                            type="text"
                            required
                            placeholder="Enter name of place"
                            name="title"
                            value={this.state.title}
                            onChange={this.inputChangeHandler}
                        />
                    </Col>
                </FormGroup>

                <FormGroup controlId="title">
                    <Col componentClass={ControlLabel} sm={2}>
                        Description
                    </Col>
                    <Col sm={10}>
                        <FormControl
                            disabled={this.checkStateInput()}
                            type="text"
                            required
                            placeholder="Enter description of place"
                            name="description"
                            value={this.state.description}
                            onChange={this.inputChangeHandler}
                        />
                    </Col>
                </FormGroup>

                <FormGroup controlId="photo">
                    <Col componentClass={ControlLabel} sm={2}>
                        Photo
                    </Col>
                    <Col sm={10}>
                        <FormControl
                            type="file"
                            name="image"
                            onChange={this.fileChangeHandler}
                        />
                    </Col>
                </FormGroup>

                <FormGroup>
                    <Col smOffset={2} sm={10}>
                        <Button
                            bsStyle="primary"
                            type="submit"
                            disabled={this.checkState()}
                        >
                            Save
                        </Button>
                    </Col>
                </FormGroup>

                <FormGroup>
                    <Col smOffset={2} sm={10}>
                        <label htmlFor="agree" style={{marginRight: '10px'}}>I understand</label>
                        <input onChange={this.checkInput} type="checkbox" id='agree'/>
                    </Col>
                </FormGroup>
            </Form>
        )
    }
}

const mapStateToProps = state => {
    return {
        user: state.users
    }
};

const mapDispatchToProps = dispatch => {
    return {
        addNewPlace: (place) => dispatch(addNewPlace(place)),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddPlace);