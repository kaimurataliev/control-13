import React from 'react';
import {connect} from 'react-redux';
import {Route, Switch, withRouter} from 'react-router-dom';

import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import AddPlace from './containers/AddPlace/AddPlace';
import AllPlaces from './containers/AllPlaces/AllPlaces';
import MoreInfo from './containers/MoreInfo/MoreInfo';

const Routes = () => {
    return (
        <Switch>
            <Route path='/' exact component={AllPlaces} />
            <Route path="/register" exact component={Register}/>
            <Route path="/login" exact component={Login}/>
            <Route path="/add-place" component={AddPlace}/>
            <Route path='/moreInfo/:id' component={MoreInfo} />
        </Switch>
    )
};

const mapStateToProps = state => {
    return {
        user: state.users.user
    }
};

export default withRouter(connect(mapStateToProps)(Routes));